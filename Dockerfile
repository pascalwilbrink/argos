FROM frolvlad/alpine-oraclejdk8:slim
EXPOSE 8761
ADD target/argos-0.0.1-SNAPSHOT.jar /argos.jar
ENTRYPOINT ["java","-jar","/argos.jar"]
